package main;


import java.net.URI;
import java.net.URISyntaxException;

import javax.websocket.SendHandler;
import javax.websocket.SendResult;

import org.json.JSONObject;


public class PointCloudTest {

    public static class InitCallback implements WebSocketIO.Callback {
		public void callback(WebSocketIO wsio, JSONObject d) {
			System.out.println("init callback");
			while (true) {
//				wsio.emitString("newPointCloudFrame", "{\"values\": [ [1,1,1], [2,2,2], [3,3,3] ]}");
				wsio.emitString("newPointCloudFrame", "{\"values\": [ \"0.0 1.1 1.1\",\"2.0 2.0 2.0\",\"3.0 3.0 3.0\",\"4.0 4.0 4.0\" ]}");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.out.println("Interrupt");
				}
			}
		}
    }

    
    
    public static class TransmitCallback implements WebSocketIO.Callback {
		public void callback(WebSocketIO wsio, JSONObject d) {
			System.out.println("transmit callback");
//			
//			SendHandler sendHandler  =new SendHandler();
			
			SendResult  sendResult = new SendResult();
			sendResult.isOK();
			
			
			while (true) {
				wsio.emitString("newPointCloudFrame", "{\"values\": [ \"0.0 1 1\",\"2.0 2.0 2.0\",\"3.0 3.0 3.0\",\"4.0 4.0 4.0\" ]}");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.out.println("Interrupt");
				}
			}
		}
    }
    

    public static class AckCallback implements WebSocketIO.Callback {
		public void callback(WebSocketIO wsio, JSONObject data) {
			System.out.println("ack received");
		}
    }
    
    public static class OpenCallback implements WebSocketIO.OpenCallback {
		public void callback(WebSocketIO wsio) {
			System.out.println("open callback");
			// FIXME: implement retry in wsio.emit
			WebSocketIO.Callback initCB = new InitCallback();
			WebSocketIO.Callback aCB = new AckCallback();	

			wsio.on("initialize", initCB);
			wsio.on("ack", aCB);
			//wsio.emitString("addClient", "{\"requests\": }");
			wsio.emitString("addClient", "{\"requests\": {} }");
			System.out.println("interrupt");
		}
    }

    public static void main(String[] args) {
        try {


	    WebSocketIO.OpenCallback openCallback = new OpenCallback();
            // open websocket
//            final WebSocketIO clientEndPoint = new WebSocketIO(new URI("ws://10.234.2.22:9280"), openCallback);
//            final WebSocketIO clientEndPoint = new WebSocketIO(new URI("ws://127.0.0.1:9292"), openCallback);
            // other server
            final WebSocketIO clientEndPoint = new WebSocketIO(new URI("ws://10.234.2.22:8901"), openCallback);
            // send message to websocket
//            clientEndPoint.emitMessage("{'event':'addPointCloud','channel':'test'}");

            // wait 5 seconds for messages from websocket
            Thread.sleep(10000);

        } catch (InterruptedException ex) {
            System.err.println("InterruptedException exception: " + ex.getMessage());
        } catch (URISyntaxException ex) {
            System.err.println("URISyntaxException exception: " + ex.getMessage());
        }
    }
    
    
    
}

package main;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.*;

@ClientEndpoint
public class WebSocketIO {

	Session userSession = null;
	HashMap<String, String> localListeners, remoteListeners;
	Integer aliasCount;
	Map<String,Callback> messages;
	// this cannot be used for sending messages directly
	WebSocketContainer ws;
	OpenCallback openCallback;

        public interface OpenCallback
        {
            void callback(WebSocketIO wsio);
        }

        public interface Callback
        {
            void callback(WebSocketIO wsio, JSONObject d);
        }

	public WebSocketIO(URI endpointURI, OpenCallback onOpen) {
		try {
			this.openCallback = onOpen;

			ws = ContainerProvider.getWebSocketContainer();

			messages = new HashMap<String,Callback>();

			// Initialising listeners
			remoteListeners = new HashMap<>();
			remoteListeners.put("#WSIO#addListener", "0000");
			// FIXME: this is a hack
			remoteListeners.put("addClient", "0001");

			localListeners = new HashMap<>();
			localListeners.put("0000", "#WSIO#addListener");


			aliasCount = 1;
			ws.connectToServer(this, endpointURI);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

        /**
        * Set a message handler for a given name
        *
        * @method on
        * @param name {String} name for the handler
        * @param callback {Function} handler to be called for a given name
        */
        void on(String name, Callback callback)
        {
        	System.out.println("setting callback for ``"+name+"''");
            // pad with leading zeros to length 4
            //String alias = this.aliasCount.toString("X4");
            String alias = String.format("%04d",aliasCount);
            this.localListeners.put(alias, name);
            this.messages.put(name, callback);
            this.aliasCount++;
            //String message = new JavaScriptSerializer().Serialize(obj);
            String message = "{\"listener\": \""+name+"\", \"alias\": \""+alias+"\"}";
            String addListenerAlias = "#WSIO#addListener";
            System.out.println("on... addListener");
            this.emitString(addListenerAlias, message);
        }

        public class BasicMsg
        {
            public String f;
            public Object d;
            
            public String printJsonString(String object1, String object2){
    			JSONObject json = new JSONObject();
    			json.put(object1, f);
    			json.put(object2, d);
//    			System.out.println(json.toString());
    			return  json.toString();
    		}
            
        }

        void emitString(String name, String dataString)
        {
            String alias = this.remoteListeners.get(name);

            BasicMsg m = new BasicMsg();
            m.f = alias;
            m.d = dataString;

//            String message = new JavaScriptSerializer().Serialize(obj);
            String message = "{\"f\":\""+alias+"\",\"d\":"+dataString+"}";

            //ws.Send(message);
	    emitMessage(message);	
        }

	/**
	 * Callback for Connection open events.
	 */
	@OnOpen
	public void onOpen(Session userSession, EndpointConfig config) {
		System.out.println("onOpen websocket");
		if (this.openCallback == null) {
			System.out.println("Warning: onOpen is null");
		}
		this.userSession = userSession;
		this.openCallback.callback(this);
		
	}

	/**
	 * Callback for Connection close events.
	 */
	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		System.out.println("onClose websocket");
		this.userSession = null;
	}

	/**
	 * Callback for Message Events when received.
	 */
	@OnMessage
	public void onMessage(String message) {
		System.out.println("Message received: " + message);
		JSONObject jsonObject = new JSONObject(message);
		String f = (String)jsonObject.getString("f");
		JSONObject d = jsonObject.getJSONObject("d");

		System.out.println("F is: "+ f);
		
		String fName = this.localListeners.get(f);
		System.out.println("fName is: "+ fName);
		if (fName == "#WSIO#addListener")
		{
		    String listener = d.getString("listener");
		    String alias = d.getString("alias");
		    this.remoteListeners.put(listener,alias);
		    System.out.println(listener+" -> "+alias);
		    return;
			
			
			
		} else {
		    Callback cb = this.messages.get(fName);
		    cb.callback(this,d);
		}

	}

	/**
	 * Emit a message. 
	 */
	public void emitMessage(String message) {
		System.out.println("emitMessage: "+message);
		// Tests format of the json message
		JSONObject jsonObject = new JSONObject(message);
		this.userSession.getAsyncRemote().sendText(message);
	}

	
}

